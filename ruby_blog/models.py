from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False, verbose_name='Имя автора')

    def __str__(self) -> str:
        return self.name

# Create your models here. #true у поля body - просто для наглядности
# created_at - дата создания статьи
class Post(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name='Заголовок')
    body = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Текст статьи') 
    author = models.ForeignKey(to=Author, on_delete=models.CASCADE, null=False, verbose_name = 'Автор')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Время изменения')

    def __str__(self):
        return f"#{self.pk} - {self.title}"