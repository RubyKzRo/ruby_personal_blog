from django import forms
from django.forms import widgets
from .models import Author

class PostForm(forms.Form):
    title = forms.CharField(
        max_length=200, 
        required=True, 
        label="Заголовок*",
        widget=widgets.TextInput(attrs={
            'class': 'form-control'
            })
        )
    body = forms.CharField(
        max_length=3000, 
        required=False, 
        label="Текст",
        widget=widgets.Textarea(attrs={
            'class': 'form-control', 
            'rows': 3
            })
        )
    author = forms.ModelChoiceField(
        required=True, 
        label="Автор*", 
        widget=widgets.Select(attrs={
            'class': 'form-select',
            'placeholder': 'Author'
            }),
        queryset=Author.objects.all()
        )

class AuthorForm(forms.Form):
    name = forms.CharField(
        max_length=200, 
        required=True, 
        label="Автор*",
        widget=widgets.TextInput(attrs={
            'class': 'form-control', 
            'placeholder': 'Author name'
            })
    )