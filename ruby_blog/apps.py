from django.apps import AppConfig


class RubyBlogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ruby_blog'
