from django.shortcuts import render, redirect, get_object_or_404
from ..models import Post, Author
from ..forms import PostForm

# from django.urls import reverse
# from django.http import HttpResponseRedirect
# from django.templatetags.static import static


def index_view(request):
    # url = reverse('post_detail', kwargs={'pk':1})
    # print(url)
    return render(request, 'index.html')

def post_list_view(request):
    posts = Post.objects.all()
    return render(request, 'posts/list.html', context={
        'posts': posts
    })

# def post_detail_view(request, *args, **kwargs):
#     try:
#         post = Post.objects.get(pk=kwargs.get('pk'))
#     except Post.DoesNotExist as error:
#         raise Http404

#     return render(request, 'post_detail.html',
#                   context={'post': post })

# с шорткатом - оптимизация кода, где try except отлов ошибки под капотом:
def post_detail_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))
    return render(request, 'posts/detail.html',
                  context={'post': post})

# post create БЕЗ Джанго форм
# def post_create_view(request):
#     if request.method == 'GET':
#         return render(request, 'post_create.html')
#     elif request.method == 'POST':
#         title=request.POST.get('title')
#         body=request.POST.get('body')
#         author=request.POST.get('author')
#         errors = dict()
#         if not title:
#             errors['title'] = 'Title should not be empty!'
#         elif len(title) > 50:
#             errors['title'] = 'Title should be 50 symbols or less!'
#         if not author:
#             errors['author'] = 'Author should not be empty!'
#         elif len(title) > 50:
#             errors['title'] = 'Author should be 30 symbols or less!'
#         if len(errors) > 0:
#     #   либо:
#     #   if eroors:
#             post = Post(title=title, body=body, author=author)
#             return render(request, 'post_create.html', context={
#                 'errors': errors,
#                 'post': post
#                 })
#         else:
#             new_post = Post.objects.create(
#                 title=title,
#                 body=body,
#                 author=author
#             )
#         return redirect('home_page')

# # без форм и без валидации
# def post_create_view(request):
#     if request.method == 'GET':
#         return render(request, 'post_create.html')
#     elif request.method == 'POST':
#         new_post = Post.objects.create(
#             title=request.POST.get('title'),
#             body=request.POST.get('body'),
#             author=request.POST.get('author'),
#         )
#     return redirect('home_page')

# с Джанго формами
def post_create_view(request):
    if request.method == 'GET':
        form = PostForm()
        return render(request, 'posts/create.html', context={
            'form': form,
            'authors': Author.objects.all
        })
    elif request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            author = Author.objects.get(pk=request.POST.get('author')) 
            new_post = Post.objects.create(
                title=form.cleaned_data['title'],
                body=form.cleaned_data['body'],
                author=author
            )
            return redirect('post_list')
        else:
            return render(request, 'posts/create.html', context={
                'form': form
            })    
        
# post update БЕЗ Джанго форм (Не работает корректно)
# def post_update_view(request, *args, **kwargs):
#     post = get_object_or_404(Post, pk=kwargs.get('pk'))
#     if request.method == 'GET':
#         return render(request, 'post_update.html', context={'post': post})
#     elif request.method == 'POST':
#         errors = dict()
#         if not post.title:
#             errors['title'] = 'Title should not be empty!'
#         elif len(post.title) > 50:
#             errors['title'] = 'Title should be 50 symbols or less!'

#         if not post.author:
#             errors['author'] = 'Author should not be empty!'
#         elif len(post.author) > 50:
#             errors['title'] = 'Author should be 30 symbols or less!'

#         if errors:
#             return render(request, 'post_update.html', context={
#                 'errors': errors,
#                 'post': post
#             })
#         else:
#             post.title = request.POST.get('title')
#             post.author = request.POST.get('author')
#             post.body = request.POST.get('body')
#             post.save()
#             return redirect('post_detail', pk=post.pk)

# с Джанго формами
def post_update_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))

    if request.method == 'GET':
        form = PostForm(initial={
            'title': post.title,
            'body': post.body,
            'author': post.author           
        })
        return render(request, 'posts/update.html', context={
            'form': form, 
            'post': post
        })
    elif request.method == 'POST':
        form = PostForm(data=request.POST)
        if form.is_valid():
            post.title = form.cleaned_data.get('title')
            post.body = form.cleaned_data.get('body')
            post.author = form.cleaned_data.get('author')
            post.save()
            return redirect('post_detail', pk=post.pk)
        else:
            return render(request, 'posts/update.html', context={
                'form': form, 
                'post': post
            })
            
# # с Джанго формами
# def post_update_view(request, *args, **kwargs):
#     post = get_object_or_404(Post, pk=kwargs.get('pk'))
#     if request.method == 'GET':
#         form = PostForm(initial={
#             'title': post.title,
#             'body': post.body,
#             'author': post.author           
#         })
#         return render(request, 'posts/update.html', context={'form': form, 'post': post})
#     elif request.method == 'POST':
#             form = PostForm(data=request.POST)
#             if form.is_valid():
#                 post.title = form.cleaned_data.get('title')
#                 post.body = form.cleaned_data.get('body')
#                 post.author = form.cleaned_data.get('author')
#                 post.save()
#                 return redirect('post/detail', pk=post.pk)
#             else:
#                 return render(request, 'posts/update.html', context={
#                     'form': form, 
#                     'post': post
#                 })

# # без форм и без валидации
# def post_update_view(request, *args, **kwargs):
#     post = get_object_or_404(Post, pk=kwargs.get('pk'))
#     if request.method == 'GET':
#         return render(request, 'post_update.html', context={'post': post})
#     elif request.method == 'POST':
#             post.title = request.POST.get('title')
#             post.author = request.POST.get('author')
#             post.body = request.POST.get('body')
#             post.save()
#             return redirect('post_detail', pk=post.pk)

def post_delete_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))
    if request.method =='GET':
        return render(request, 'posts/delete.html', context={'post': post})
    elif request.method == 'POST':
        post.delete()
        return redirect('home_page')